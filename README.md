# Billing Backend
1. Run 
  ```
  npm install
  ```
2. Add .env with variables:
  ```
  DATABASE_URL="link for db"
  JWT_SECRET=TetillaPelua202369
  JWT_EXPIRATION_TIME=86400
  ```
3. Run
  ```
  npm run devstart
  ```